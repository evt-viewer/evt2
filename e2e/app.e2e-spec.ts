import { EVT2Page } from './app.po';

describe('evt2 App', () => {
  let page: EVT2Page;

  beforeEach(() => {
    page = new EVT2Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
