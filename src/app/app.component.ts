import {Component, ComponentFactoryResolver} from '@angular/core'

import {CommunicationService} from './services/communication.service'
import {ConfigService} from './config/config.service'

import {StructureService} from './data-handler/parsed-data/services/structure.service'
import {Document, Page} from './data-handler/parsed-data/models/edition-structure'

import {LocaleService, Language, Translation} from 'angular-l10n';

import {NamedEntitiesService} from './data-handler/parsed-data/services/named-entities.service'
import {NamedEntityCollection} from './data-handler/parsed-data/models/named-entity'

import {GenericParserService} from './data-handler/parsers/services/generic-parser.service'

import {DOMUtilsService} from './utils/dom-utils.service'
import {XMLUtilsService} from './utils/xml-utils.service'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./_app.component.scss']
})
export class AppComponent extends Translation {
    @Language() lang: string

    private title: string
    isLoading: boolean
    isMainMenuOpened: boolean

    secondaryContentOpened: string

    // TEST
    selectedPageText = 'SELECT_PAGE'

    constructor(public locale: LocaleService,
                public config: ConfigService,
                private communication: CommunicationService,
                public parsedStructure: StructureService,
                public parsedNamedEntities: NamedEntitiesService,
                public genericParser: GenericParserService,
                public DOMUtils: DOMUtilsService,
                public XMLUtils: XMLUtilsService) {
        super();
        this.isLoading = true
        this.isMainMenuOpened = false
        this.initialBoot()
    }

    initialBoot() {
        this.communication.getExternalConfig(this.config.data.configUrl).then((data) => {
            this.communication.getData(this.config.data.dataUrl).then(() => {
                this.title = this.config.data.indexTitle
                this.isLoading = false
            })
        })
    }

    toggleMainMenu(opened?: boolean) {
        this.isMainMenuOpened = opened !== undefined ? opened : !this.isMainMenuOpened
    }

    openSecondaryContent($event) {
        // TODO: Check if available
        this.secondaryContentOpened = $event
    }

    closeSecondaryContent() {
        this.secondaryContentOpened = undefined
    }
    // TEST

    getDocumentsCollection() {
        const documentsCollection: any = this.parsedStructure.getDocuments()
        return documentsCollection.indexes
    }

    getDocument(docId: string) {
        return this.parsedStructure.getDocument(docId)
    }

    getPage(pageId: string) {
        return this.parsedStructure.getPage(pageId)
    }

    showPageText(pageId: string, docId: string) {
        const originalXMLString = this.DOMUtils.balanceXHTML(this.getPage(pageId).text[docId].original)
        const parsedXMLString = this.XMLUtils.parseXml('<div>'+originalXMLString+'</div>')
        const parsedPage = this.genericParser.parseXMLElement(docId, parsedXMLString, {})
        this.selectedPageText = parsedPage.innerHTML
    }

    getNamedEntitiesCollection() {
        return this.parsedNamedEntities.getCollectionsIndexes()
    }

    getNamedEntityTitle(collectionId: string) {
        const collection: NamedEntityCollection = this.parsedNamedEntities.getCollectionById(collectionId)
        return collection ? collection.title : ''
    }

    getCollectionEntities(collectionId: string) {
        const collection: NamedEntityCollection = this.parsedNamedEntities.getCollectionById(collectionId)
        return collection ? collection.indexes : []
    }

    getProjectInfoTabs() {
        let tabs: any = {
            contents: {},
            indexes: []
        }
        tabs.contents.fileDescription = {
            label: 'File Description',
            name: 'fileDescription',
            content: 'File Description Content',
        };
        tabs.indexes.push('fileDescription');

        tabs.contents.encodingDescription= {
            label: 'Encoding Description',
            name: 'encodingDescription',
            content: 'Encoding Description Content',
        };
        tabs.indexes.push('encodingDescription');

        tabs.contents.textProfile = {
            label: 'Text Profile',
            name: 'textProfile',
            content: 'Text Profile Content',
        };
        tabs.indexes.push('textProfile');

        tabs.contents.outsideMetadata= {
            label: 'Outside Metadata',
            name: 'outsideMetadata',
            content: 'Outside Metadata Content',
        };
        tabs.indexes.push('outsideMetadata');
        return tabs
    }

    getNamedEntitiesTabs() {
        let entitiesCollectionIndexes = this.parsedNamedEntities.getCollectionsIndexes()
        let tabs: any = {
            contents: {},
            indexes: []
        }
        for (let i of entitiesCollectionIndexes) {
            tabs.indexes.push(i)
            let listId = i,
                collection : NamedEntityCollection = this.parsedNamedEntities.getCollectionById(i)
            tabs.contents[i] = {
                label: collection.title,
                icon: collection.listIcon,
                name: collection.id,
                content: '<evt-list [listId]="' + collection.id + '" [listType]="' + collection.listType + '"></evt-list>'
            }
        }
        return tabs
    }
}
