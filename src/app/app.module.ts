// Components will go into "declarations",
// Modules will go into "imports",
// Services will go into "providers",
// and in bootstrap we just add the AppComponent that is the root component

import {BrowserModule} from '@angular/platform-browser'
import {NgModule, APP_INITIALIZER, Injectable} from '@angular/core'
import {HttpClientModule} from '@angular/common/http'

import {
    L10nConfig,
    L10nLoader,
    LocalizationModule,
    LocaleValidationModule,
    StorageStrategy,
    ProviderType
} from 'angular-l10n'

import {JsUtilsService} from './utils/js-utils.service'
import {DOMUtilsService} from './utils/dom-utils.service'
import {XMLUtilsService} from './utils/xml-utils.service'

import {ConfigService} from './config/config.service'
import {EVTGlobalDefaultConfigService} from './config/global-default.config.service'

import {XmlTagsService} from './data-handler/parsers/services/xml-tags.service'

import { ContentViewer , EmbeddedComponents, embeddedComponents} from './components/dynamic-content-viewer'

// Parsers
import {GenericParserService} from './data-handler/parsers/services/generic-parser.service'
import {StructureParserService} from './data-handler/parsers/services/structure-parser.service'
import {NamedEntitiesParserService} from './data-handler/parsers/services/named-entities-parser.service'

// Parsed Data Collections
import {BaseDataService} from './data-handler/parsed-data/services/base-data.service'
import {StructureService} from './data-handler/parsed-data/services/structure.service'
import {NamedEntitiesService} from './data-handler/parsed-data/services/named-entities.service'

import {AppComponent} from './app.component'
import {NamedEntityComponent} from './components/named-entity/named-entity.component'

import {CommunicationService} from './services/communication.service'
import { ButtonComponent } from './components/generic-ui/button/button.component';
import { MainMenuComponent } from './components/generic-ui/main-menu/main-menu.component';
import { DialogComponent } from './components/generic-ui/dialog/dialog.component';
import { TabsContainerComponent } from './components/generic-ui/tabs-container/tabs-container.component';
import { NamedEntitiesListComponent } from './components/named-entity/named-entities-list.component';
import { NamedEntityRefComponent } from './components/named-entity/named-entity-ref/named-entity-ref.component'

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'it', dir: 'ltr' }
        ],
        defaultLocale: { languageCode: 'en', countryCode: 'US' },
        currency: 'USD',
        storage: StorageStrategy.Cookie,
        cookieExpiration: 30
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/locale-' }
        ],
        caching: true,
        composedKeySeparator: '.',
        missingValue: 'No key',
        i18nPlural: true
    }
};

// Advanced initialization.
export function initL10n(l10nLoader: L10nLoader): Function {
    return () => l10nLoader.load();
}


@NgModule({
  declarations: [
    AppComponent,
    ContentViewer,
    embeddedComponents,
    NamedEntityComponent,
    ButtonComponent,
    MainMenuComponent,
    DialogComponent,
    TabsContainerComponent,
    NamedEntitiesListComponent,
    NamedEntityRefComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LocalizationModule.forRoot(l10nConfig),
    LocaleValidationModule.forRoot()
  ],
  entryComponents: [embeddedComponents],
  providers: [
    CommunicationService,
    {
        provide: APP_INITIALIZER,
        useFactory: initL10n,
        deps: [L10nLoader],
        multi: true
    },
    EmbeddedComponents,
    JsUtilsService,
    DOMUtilsService,
    XMLUtilsService,
    ConfigService,
    EVTGlobalDefaultConfigService,
    XmlTagsService,
    GenericParserService,
    StructureParserService,
    NamedEntitiesParserService,
    BaseDataService,
    StructureService,
    NamedEntitiesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
