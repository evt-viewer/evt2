import { TestBed, inject } from '@angular/core/testing';

import { DOMUtilsService } from './domutils.service';

describe('DOMUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DOMUtilsService]
    });
  });

  it('should be created', inject([DOMUtilsService], (service: DOMUtilsService) => {
    expect(service).toBeTruthy();
  }));
});
