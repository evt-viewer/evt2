import { TestBed, inject } from '@angular/core/testing';

import { XMLUtilsService } from './xml-utils.service';

describe('XMLUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XMLUtilsService]
    });
  });

  it('should be created', inject([XMLUtilsService], (service: XMLUtilsService) => {
    expect(service).toBeTruthy();
  }));
});
