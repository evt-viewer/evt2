import {Injectable} from '@angular/core'
/**
 * Service that exposes useful methods
 */
@Injectable()
export class JsUtilsService {
  /**
    * JsUtilsService constructor
    */
  constructor() {}
  /**
    * Extend an object with another:
    * - add to destination all properties that only appears in source
    * - overwrite all properties that appears in destination with the corresponding
    * properties that appear in source.
    *
    * @param destination JSON object containing an object to extend
    * @param source JSON object containing the extension to add to destination
    *
    * @returns Extended JSON object
    */
  deepExtend(destination: object, source: object): object {
    for (const property of this.keys(source)) {
      if (source[property] && source[property].constructor && source[property].constructor === Object) {
        destination[property] = destination[property] || {}
        destination[property] = source[property]
      } else {
        destination[property] = this.deepCopy(source[property])
      }
    }
    return destination;
  }

  /**
    *  Extend an object with another, skipping the undefined values:
    * - add to destination all properties that only appears in source
    * - overwrite all properties that appears in destination with the corresponding properties that appear in source
    * - skip undefined properties that are undefined in source object
    *
    * @param destination JSON object containing an object to extend
    * @param source JSON object containing the extension to add to destination
    * @param boolean Boolean that tells the function whether to skip undefined properties or not
    *
    * @returns Extended JSON object
    */
  deepExtendSkipDefault(destination: object, source: object, skipDefault?: boolean): object {
    for (const property of this.keys(source)) {
      if (source[property] && source[property].constructor && source[property].constructor === Object) {
        destination[property] = destination[property] || {}
        destination[property] = source[property]
      } else {
        if (!skipDefault) {
          destination[property] = this.deepCopy(source[property])
        } else {
          if (property === 'dataUrl') {
            if (source[property] !== '') {
              destination[property] = this.deepCopy(source[property])
            }
          } else {
            if (source[property] === 'NONE' || source[property] === 'NULL') {
              destination[property] = ''
            } else if (source[property] !== '') {
              destination[property] = this.deepCopy(source[property])
            }
          }
        }
      }
    }
    return destination
  }

  /**
    * Copy each property of a given object into a new object
    *
    * @param objectToCopy JSON object to be copied
    *
    * @returns Copied JSON object
    */
  deepCopy(objectToCopy: object): object {
    let newObj = objectToCopy
    if (objectToCopy && typeof objectToCopy === 'object') {
      newObj = Object.prototype.toString.call(objectToCopy) === '[object Array]' ? [] : {}
      for (const i in this.keys(objectToCopy)) {
        if (objectToCopy[i]) {
          newObj[i] = this.deepCopy(objectToCopy[i])
        }
      }
    }
    return newObj
  }
  /**
    * Return all Object keys
    *
    * @param objectToHandle JSON object to handle
    *
    * @returns Array containing keys of given object
    */
  keys(objectToHandle: object): string[] {
    return Object.keys(objectToHandle)
  }

  /**
   * Generate a Regular Expression with given element definitions:
   * - It checks if there is an attribute, introduced by a '['
   *  - If there isn't a square bracket, it adds the element name in the match string
   * - Otherwise it saves the name of the element marked by the '<' and the '['
   * - Adds regular expression operators to the match string
   * - Looks for the closing square bracket and for the equals sign
   * - Adds the name of the attribute , the regular expression operators and and the value of the attribute
   *
   * @param def string of the element definition, contained in config file.
   * It may contain more than one definition separated by commas
   *
   * @returns The regular expression created
   *
   * @author CM
   */
  createRegExpr(def: string): RegExp {
    let match = '('
    // def may contain more than one definition separated by commas
    // Save all the definition contained in def in aDef array
    const aDef = def.split(',')

    for (let i = 0; i < aDef.length; i++) {
      // Checks if there is an attribute, introduced by a '['
      if (aDef[i].indexOf('[') < 0) {
        // If there isn't a square bracket, it adds the element name in the match string
        match += aDef[i].replace(/[>]/g, '')
      } else {
        // Otherwise it saves the name of the element marked by the '<' and the '['
        const bracketOpen = aDef[i].indexOf('[')
        if (aDef[i].substring(1, bracketOpen) !== '[') {
          match += aDef[i].substring(0, bracketOpen)
        }
        // Adds regular expression operators to the match string
        match += '[^<>]*?'
        // Looks for the closing square bracket
        const bracketClose = aDef[i].indexOf(']')
        // ...and for the equals sign
        const equal = aDef[i].indexOf('=')
        // Adds the name of the attribute...
        match += aDef[i].substring(bracketOpen + 1, equal)
        // ...reg expr operators
        match += '\\s*?=\\s*?[\'\"]\\s*?'
        // ...and the value of the attribute
        match += aDef[i].substring(equal + 1, bracketClose)
      }
      if (i < aDef.length - 1) {
        // Adds operator or to add a new definition contained in aDef
        match += '|'
      } else if (i === aDef.length - 1) {
        // Closes the regular expression
        match += ')'
      }
    }
    return new RegExp(match, 'i')
  }

  /**
   * Capitalize first letter or all letters of a given string.
   *
   * @param str String to handle
   * @param all Whether to capitalize all letters of just first one
   *
   * @returns Capitalized string
   * @author CDP
   */
  capitalize(str: string, all?: boolean): string {
    const reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/
    return (!!str) ? str.replace(reg, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    }) : ''
  }
  /**
   * Transform given string from camel notation to space notation.
   *
   * @param str String to handle
   *
   * @returns Transformed string
   * @author CDP
   */
  camelToSpace(str: string): string {
    return (!!str) ? str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2') : ''
  }
  /**
   * Transform given string from camel notation to underscore notation.
   *
   * @param str String to handle
   *
   * @returns Transformed string
   * @author CDP
   */
  camelToUnderscore(str: string): string {
    return (!!str) ? str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1_$2') : ''
  }

  removeTagBrackets(tagName: string): string {
    return tagName.replace(/[<>]/g, '')
  }
}
