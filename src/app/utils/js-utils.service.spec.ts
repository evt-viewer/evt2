import { TestBed, inject } from '@angular/core/testing';

import { JsUtilsService } from './js-utils.service';

describe('JsUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JsUtilsService]
    });
  });

  it('should be created', inject([JsUtilsService], (service: JsUtilsService) => {
    expect(service).toBeTruthy();
  }));
});
