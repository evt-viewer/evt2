import {Injectable} from '@angular/core'

declare var window: any

/**
 * Service that exposes useful methods for XML parsing
 */
@Injectable()
export class XMLUtilsService {
  /**
    * XMLUtilsService constructor
    */
  constructor() {
    if (typeof window.DOMParser !== 'undefined') {
      this.parseXml = function(xmlStr: string) {
        return (new window.DOMParser()).parseFromString(xmlStr, 'text/xml')
      }
    } else if (typeof window.ActiveXObject !== 'undefined' &&
      new window.ActiveXObject('Microsoft.XMLDOM')) {
      this.parseXml = function(xmlStr: string) {
        const xmlDoc = new window.ActiveXObject('Microsoft.XMLDOM')
        xmlDoc.async = 'false'
        xmlDoc.loadXML(xmlStr)
        return xmlDoc
      }
    } else {
      throw new Error('No XML parser found')
    }
  }
  parseXml(xmlStr: string) : any {}
}
