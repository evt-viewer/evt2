import { Injectable, Inject } from '@angular/core'

import { EVTBaseConfig } from './base.config'
import { EVTGlobalDefaultConfigService } from './global-default.config.service'

import { JsUtilsService } from '../utils/js-utils.service'

@Injectable()
export class ConfigService {
  data: any = {}

  constructor(
    private globalDefaultConfig: EVTGlobalDefaultConfigService,
    private jsUtils: JsUtilsService
  ) {
    this.jsUtils.deepExtend(this.data, this.globalDefaultConfig)
  }

  makeDefaults(name: string, options: object) {
    const defaults: object = this.jsUtils.deepCopy(EVTBaseConfig)

    // Extend the very basic defaults with given options
    if (typeof (options) !== 'undefined') {
      this.jsUtils.deepExtendSkipDefault(defaults, options, false)
    }

    // Try to see if there's a provided configuration for this module,
    // and use it to extend our options
    if (typeof (this.data.modules[name]) !== 'undefined') {
      this.jsUtils.deepExtendSkipDefault(defaults, this.data.modules[name], false)
      // console.log('BaseComponent extending with Config.modules conf')
    }

    // Save the built configuration into the Config with our name
    this.data.modules[name] = defaults

    return defaults
  }

  isValid() {
    // TODO: set the rules for checking
    return true
  }

  isModuleActive(moduleName: string) {
    return (typeof this.data.modules[moduleName] === 'object') && this.data.modules[moduleName].active === true
  }

  extendDefault(json: any) {
    this.jsUtils.deepExtendSkipDefault(this.data, json, true)
  }
}
