import {Injectable} from '@angular/core';

import {ListsTags} from '../models/xml-tags/lists-tags'
import {RelationsTags} from '../models/xml-tags/relations-tags'
import {PageTags, DocTags, DocTagName, GenericTagSet} from '../models/xml-tags/structure-tags'
/**
 * Service that is used to set, store and retrieve XML tag definitions.
 * It is prepared to handle custom XML tag configuration.
 */
@Injectable()
export class XmlTagsService {
  /**
   * Definitions regarding pages
   */
  private pages: PageTags
  /**
   * Definitions regarding documents
   */
  private docs: DocTags
  /**
   * Definition regarding content edition.
   */
  private contentEditionDef: string
  /**
   * Definition regarding front.
   */
  private frontDef: string
  /**
   * Definition regarding bibliography.
   */
  private biblDef: GenericTagSet
  /**
   * Definitions regarding lists.
   */
  private lists: ListsTags
  private namedEntities: string[]
  /**
   * Definitions regarding relations.
   */
  private relations: RelationsTags
  /**
   * XmlTagsService constructor.
   * It sets definitions regarding entities lists and entities relations.
   */
  constructor() {
    this.initStructure()
    this.initLists()
    this.initRelations()
    this.biblDef = {
      tagName: '<biblStruct>',
      idAttr: 'xml:id'
    }
    this.frontDef = '<front>'
  }
  /**
   * Sets definitions regarding entities lists.
   */
  initLists() {
    this.lists = {
      extContainer: '<sourceDesc>',
      idAttribute: 'xml:id',
      typeAttribute: 'type',
      headerTag: '<head>',
      whatToParse: [{
        listDef: '<listPlace>',
        listType: 'place',
        contentDef: '<place>',
        contentForLabelDef: '<placeName>'
      }, {
        listDef: '<listPerson>',
        listType: 'person',
        contentDef: '<person>',
        contentForLabelDef: '<persName>'
      }, {
        listDef: '<listOrg>',
        listType: 'org',
        contentDef: '<org>',
        contentForLabelDef: '<orgName>'
      }, {
        listDef: '<list>',
        listType: 'generic',
        contentDef: '<item>',
        contentForLabelDef: ''
      }]
    }
    this.namedEntities = []
    for (var i = 0; i < this.lists.whatToParse.length; i++) {
      var labelDef = this.lists.whatToParse[i].contentForLabelDef
      if (labelDef !== '') {
          this.namedEntities.push(labelDef.replace(/[<>]/g, '').toLowerCase())
      }
    }
  }
  /**
   * Sets definitions regarding structure elements (documents, pages, edition, ...).
   */
  initStructure() {
    this.pages = {
      tagName: '<pb>',
      idAttr: 'xml:id',
      numAttr: 'n',
      edAttr: 'ed',
      facsAttr: 'facs'
    }
    // Set Tags for Documents
    this.docs = {
      tagNames: [],
      idAttr: 'xml:id',
      docDef: '',
      numAttr: 'n'
    }
    this.docs.tagNames.push({
      docTag: 'text group text',
      editionTag: '<body>'
    })
    this.docs.tagNames.push({
      docTag: 'text',
      editionTag: '<body>'
    })
    this.docs.tagNames.push({
      docTag: 'div[subtype="edition_text"]',
      editionTag: '<div>'
    })
    this.docs.docDef = this.docs.tagNames.length > 0 ? this.docs.tagNames[0].docTag : ''
    this.contentEditionDef = '<body>'
  }
  /**
   * Sets definitions regarding entities relations.
   */
  initRelations() {
    this.relations = {
      idAttribute: 'xml:id',
      listTag: '<listRelation>',
      relationTag: '<relation>',
      nameAttribute: 'name',
      activeAttribute: 'active',
      passiveAttribute: 'passive',
      mutualAttribute: 'mutual',
      typeAttribute: 'type'
    }
  }

  /**
   * Get given set of definitions.
   * @param setName Name of definition set to retrieve
   * @returns Object representing the collection of definition for given set
   */
  getTagSet(setName: string): any {
    return this[setName];
  }
  /**
   * Update given set of definitions.
   * @param setName Name of definition set to update
   * @param newDefs New set of definitions
   * @returns Updated set
   */
  updateTagSet(setName: string, newDefs: any): any {
    return this[setName] = newDefs
  }

  updatePropertyInSet(setName: string, propertyName: string, newValue: any) {
    if (this[setName]) {
      this[setName][propertyName] = newValue
    }
  }
}
