import { TestBed, inject } from '@angular/core/testing';

import { StructureParserService } from './structure-parser.service';

describe('StructureParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StructureParserService]
    });
  });

  it('should be created', inject([StructureParserService], (service: StructureParserService) => {
    expect(service).toBeTruthy();
  }));
});
