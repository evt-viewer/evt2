import { Injectable, Inject, forwardRef } from '@angular/core'

import { Attributes } from '../../models/shared'

import { XmlTagsService } from './xml-tags.service'

@Injectable()
export class GenericParserService {
  constructor(
    private xmlTags: XmlTagsService) { }

  parseElementAttributes(element: any) {
    const attributes: Attributes = {
      values: {},
      indexes: []
    }
    if (element && element.attributes) {
      for (let i = 0; i < element.attributes.length; i++) {
        const attrib = element.attributes[i]
        if (attrib.specified) {
          const attribName = attrib.name.replace(':', '_')
          attributes.values[attribName] = attrib.value
          attributes.indexes.push(attribName)
        }
      }
    }
    return attributes
  }

  parseXMLElement(doc, element, options) {
    let newElement
    const skip = options.skip || ''
    const exclude = options.exclude || undefined

    if (element.nodeType === 3) { // Text
      newElement = element;
      if (newElement.textContent) {
        newElement.textContent = newElement.textContent.replace('__SPACE__', ' ')
      }
    } else if (element.tagName !== undefined && skip.toLowerCase().indexOf('<' + element.tagName.toLowerCase() + '>') >= 0) {
      newElement = element
    } else if (element.tagName !== undefined && exclude !== undefined &&
      exclude.toLowerCase().indexOf('<' + element.tagName.toLowerCase() + '>') >= 0) {
      newElement = document.createTextNode('')
    } else {
      const tagName = element.tagName !== undefined ? element.tagName.toLowerCase() : ''
      if (element.attributes !== undefined &&
        element.attributes.copyOf !== undefined &&
        element.attributes.copyOf.value !== '') {
        newElement = document.createElement('span')
        newElement.className = element.tagName + ' copy'
        const copyOfId = element.attributes.copyOf.value.replace('#', '')
        const match = '<' + element.tagName + ' xml:id="' + copyOfId + '.*<\/' + element.tagName + '>'
        const sRegExInput = new RegExp(match, 'ig')
        const copiedElementText = doc.outerHTML.match(sRegExInput)

        if (copiedElementText) {
          // TODO
          // const copiedElement = angular.element(copiedElementText[0])[0];
          // newElement.appendChild(parser.parseXMLElement(doc, copiedElement, options));
        }
      } else {
        // if (!parsedData.getEncodingDetail('usesLineBreaks') && tagName === 'l') {
				// 	newElement = parser.parseLine(element);
				// } else if (tagName === 'note' && skip.indexOf('<evtNote>') < 0) {
				// 	newElement = parser.parseNote(element);
				// } else if (tagName === 'date' && (!element.childNodes || element.childNodes.length <= 0)) { //TEMP => TODO: create new directive
				// 	newElement = document.createElement('span');
				// 	newElement.className = element.tagName;
				// 	var textContent = '';
				// 	for (var i = 0; i < element.attributes.length; i++) {
				// 		var attrib = element.attributes[i];
				// 		if (attrib.specified) {
				// 			if (attrib.name !== 'xml:id') {
				// 				var date = new Date(attrib.value);
				// 				var formattedDate = date && !isNaN(date) ? date.toLocaleDateString() : attrib.value;
				// 				textContent += parser.camelToSpace(attrib.name.replace(':', '-')).toLowerCase() + ': ' + formattedDate + ', ';
				// 			}
				// 		}
				// 	}
				// 	newElement.textContent = textContent.slice(0, -1);
        //
				// } else
        if (this.xmlTags.getTagSet('namedEntities').indexOf(tagName) >= 0
            //config.namedEntitiesSelector &&
				    //possibleNamedEntitiesDef.toLowerCase().indexOf('<' + tagName + '>') >= 0 &&
				    && element.getAttribute('ref') !== undefined
          ) { //TODO: Rivedere
            newElement = this.parseNamedEntityReference(doc, element, skip)
				} else {
          // TODO: Add cases
          newElement = document.createElement('span')
          newElement.className = element.tagName !== undefined ? element.tagName : ''

          // Parse Attributes
          // For each attribute it will create a data-* attribute
          if (element.attributes) {
            for (let k = 0; k < element.attributes.length; k++) {
              const attribK = element.attributes[k]
              if (attribK.specified) {
                if (attribK.name !== 'xml:id') {
                  newElement.setAttribute('data-' + attribK.name.replace(':', '-'), attribK.value)
                }
              }
            }
          }
          // Parse Child Nodes
          if (element.childNodes) {
            for (let j = 0; j < element.childNodes.length; j++) {
              const childElement = element.childNodes[j].cloneNode(true)
              newElement.appendChild(this.parseXMLElement(doc, childElement, options))
            }
          } else {
            newElement.innerHTML = element.innerHTML + ' ';
          }

          if (options.context && options.context === 'projectInfo') {
            // TODO
          }

          if (tagName === 'lb') {
            // TODO
          }
        }
      }

    }
    if (element.nodeType === 3) {
      return newElement
    } else
    if (newElement.innerHTML && newElement.innerHTML.replace(/\s/g, '') !== '') {
      return newElement
    } else {
      return document.createTextNode('')
    }
  }

  parseNamedEntityReference(doc: string, entityNode: any, skip: string) {
    var entityElem = document.createElement('evt-named-entity-ref'),
			entityRef = entityNode.getAttribute('ref'),
			entityId = entityRef ? entityRef.replace('#', '') : undefined;
    if (entityId && entityId !== '') {
			entityElem.setAttribute('data-entity-Id', entityId);
		}
		var listType = entityNode.tagName ? entityNode.tagName : 'generic';
		entityElem.setAttribute('data-entity-type', listType);

		var entityContent = '';
		for (var i = 0; i < entityNode.childNodes.length; i++) {
			var childElement = entityNode.childNodes[i].cloneNode(true),
				parsedXmlElem;

			parsedXmlElem = this.parseXMLElement(doc, childElement, {
				skip: skip
			});
			entityElem.appendChild(parsedXmlElem);
		}
		return entityElem;
  }
}
