import {Injectable} from '@angular/core'

import {XmlTagsService} from './xml-tags.service'
import {DOMUtilsService} from '../../../utils/dom-utils.service'
import {JsUtilsService} from '../../../utils/js-utils.service'

import {GenericParserService} from './generic-parser.service'
import {NamedEntitiesService} from '../../parsed-data/services/named-entities.service'

// Models
import {NamedEntityCollection, NamedEntity, NamedEntityOccurrence} from '../../parsed-data/models/named-entity'
import {ListsTags} from '../models/xml-tags/lists-tags'
import {RelationsTags} from '../models/xml-tags/relations-tags'

@Injectable()
export class NamedEntitiesParserService {
  private listsTags: ListsTags
  private relationsTags: RelationsTags

  constructor(
    private xmlTags: XmlTagsService,
    private DOMUtils: DOMUtilsService,
    private JsUtils: JsUtilsService,
    private genericParser: GenericParserService,
    public parsedNamedEntities: NamedEntitiesService) {
    this.listsTags = this.xmlTags.getTagSet('lists')
    this.relationsTags = this.xmlTags.getTagSet('relations')
  }

  parseEntities(currentDocument: any) {
    let relationsInListDef = ''

    const listsToParse = this.listsTags.whatToParse
    for (let i = 0; i < listsToParse.length; i++) {
      const listDef = this.listsTags.extContainer.replace(/[<>]/g, '') + ' > ' + listsToParse[i].listDef.replace(/[<>]/g, '')
      const contentDef = listsToParse[i].contentDef.replace(/[<>]/g, '')
      const listType = listsToParse[i].listType || 'generic'
      const listTitle = 'LISTS.' + listType.toUpperCase()
      relationsInListDef += listDef + ' ' + this.relationsTags.relationTag.replace(/[<>]/g, '') + ', '

      const listsElements = currentDocument.querySelectorAll(listDef)
      for (const element of listsElements) {
        // We consider only first level lists; inset lists will be considered differently
        if (!this.DOMUtils.isNestedInElem(element, element.tagName)) {
          const listId = element.getAttribute(this.listsTags.idAttribute) || this.parsedNamedEntities.generateCollectionId(element)
          if (listType !== 'generic' || (listType === 'generic' && listId !== undefined)) { // Generic lists only allowed if have an id
            let collection: NamedEntityCollection = {
              id: listId || this.parsedNamedEntities.generateCollectionId(element),
              listType: listType,
              title: listTitle,
              listIcon: '',
              indexes: [],
              listKeys: [],
              entities: {}
            }
            for (const child of element.childNodes) {
              if (child.nodeType === 1) {
                collection = this.parseCollectionData(child, collection)
                let el: NamedEntity
                if (listsToParse[i].listDef.indexOf('<' + child.tagName + '>') >= 0) {
                  // Parse Direct Sub list
                  this.parseDirectSubList(child, listsToParse[i], collection)
                } else if (contentDef.indexOf(child.tagName) >= 0) {
                  el = this.parseEntity(child, listsToParse[i], collection)
                  this.parsedNamedEntities.addNamedEntityInCollection(collection, el)
                }
              }
            }
            // element.parentNode.removeChild(element);
          }
        }
      }
    }
    // Parse relations
    const relations = currentDocument.querySelectorAll(relationsInListDef.slice(0, -2))
    if (relations && relations.length > 0) {
      const relCollection = {
        id: 'parsedRelations',
        listType: 'relation',
        title: 'List of Relations',
        listIcon: '',
        indexes: [],
        listKeys: [],
        entities: {}
      };
      for (const relElement of relations) {
        this.parseRelationsInList(relElement, relCollection)
      }
    }
    console.log('## parseEntities ##', JSON.parse(JSON.stringify(this.parsedNamedEntities.getCollections())))
  }

  parseDirectSubList(nodeElem: any, listToParse: any, collection: NamedEntityCollection) {
    const contentDef = listToParse.contentDef
    const listDef = listToParse.listDef
    for (const child of nodeElem.childNodes) {
      if (child.nodeType === 1) {
        collection = this.parseCollectionData(child, collection)
        let el: NamedEntity
        if (contentDef.indexOf(child.tagName) >= 0) {
          el = this.parseEntity(child, listToParse, collection)
          this.parsedNamedEntities.addNamedEntityInCollection(collection, el)
        }
      }
    }
  }

  parseSubEntity(nodeElem, contentDef, listDef) {
    const newNodeElem = document.createElement('evt-named-entity-ref')
    const entityRef = nodeElem.getAttribute('ref')
    const entityId = entityRef ? entityRef.replace('#', '') : undefined
    if (entityId && entityId !== '') {
      newNodeElem.setAttribute('data-entity-id', entityId);
    }
    const listType = nodeElem.tagName ? nodeElem.tagName : 'generic'
    newNodeElem.setAttribute('data-entity-type', listType);

    const entityContent = ''
    for (let i = 0; i < nodeElem.childNodes.length; i++) {
      const childElement = nodeElem.childNodes[i].cloneNode(true)
      let parsedXmlElem;

      if (childElement.nodeType === 1 && listDef.toLowerCase().indexOf('<' + childElement.tagName.toLowerCase() + '>') >= 0) {
        parsedXmlElem = this.parseNamedEntitySubList(childElement, childElement, '<evtNote>')
      } else {
        parsedXmlElem = this.genericParser.parseXMLElement(childElement, childElement, {skip: '<evtNote>'})
      }
      newNodeElem.appendChild(parsedXmlElem)
    }
    return newNodeElem
  }

  parseNamedEntitySubList(doc, entityNode, skip) {
    const newNodeElem = document.createElement('span')
    const entityHeadElem = document.createElement('span')
    let headTextContent = ''
    newNodeElem.className = entityNode.tagName.toLowerCase()
    entityHeadElem.className = entityNode.tagName.toLowerCase() + '-attributes'

    for (let i = 0; i < entityNode.attributes.length; i++) {
      const attrib = entityNode.attributes[i]
      if (attrib.specified) {
        newNodeElem.setAttribute('data-' + attrib.name, attrib.value)
        headTextContent += this.JsUtils.camelToSpace(attrib.value) + ', '
      }
    }
    if (headTextContent !== '') {
      entityHeadElem.textContent = headTextContent.slice(0, -2)
      newNodeElem.appendChild(entityHeadElem)
    }

    for (let j = 0; j < entityNode.childNodes.length; j++) {
      const childElement = entityNode.childNodes[j].cloneNode(true)
      const parsedXmlElem = this.genericParser.parseXMLElement(doc, childElement, {skip: skip})
      newNodeElem.appendChild(parsedXmlElem)
    }
    return newNodeElem
  }

  parseRelationsInList(nodeElem, collection: NamedEntityCollection) {
    const parsedRelation = this.genericParser.parseXMLElement(nodeElem, nodeElem, {skip: '<evtNote>'})
    const activeRefs = nodeElem.getAttribute(this.relationsTags.activeAttribute)
    const mutualRefs = nodeElem.getAttribute(this.relationsTags.mutualAttribute)
    const passiveRefs = nodeElem.getAttribute(this.relationsTags.passiveAttribute)

    let relationName = nodeElem.getAttribute(this.relationsTags.nameAttribute)
    let relationType = nodeElem.getAttribute(this.relationsTags.typeAttribute)

    if (!relationType && nodeElem.parentNode && this.relationsTags.listTag.indexOf('<' + nodeElem.parentNode.tagName + '>') >= 0) {
      relationType = nodeElem.parentNode.getAttribute(this.relationsTags.typeAttribute)
    }
    relationName = relationName ? this.JsUtils.camelToSpace(relationName) : relationName

    let elId = nodeElem.getAttribute(this.relationsTags.idAttribute)
    if (!elId) {
      elId = this.parsedNamedEntities.generateNamedEntityId(collection, nodeElem)
    }
    const relationEl: NamedEntity = {
      id: elId,
      label: '',
      content: {
        properties: {},
        indexes: []
      },
      entityType: 'relation',
      occurrences: undefined,
      indexingLetter: '',
      xmlSource: nodeElem.outerHTML.replace(/ xmlns="http:\/\/www\.tei-c\.org\/ns\/1\.0"/g, '')
    }

    relationEl.content.properties.name = [{
      attributes: {
        values: {},
        indexes: []
      },
      text: relationName
    }]
    relationEl.content.indexes.push('name')

    // Relation Label => NAME (TYPE): TEXT [TEXT is set at the bottom of the function]
    relationEl.label = relationName ? relationName.toLowerCase() : ''
    if (relationName) {
      relationEl.label += ' ('
    }
    relationEl.label += relationType ? relationType : 'generic'
    relationEl.label += ' relation'
    if (relationName) {
      relationEl.label += ')'
    }


    // relationType = relationType ? '<i>'+relationType+'</i>' : '';
    let relationText = '<span class="relation">'
    let activeText = ''
    let mutualText = ''
    let passiveText = ''

    const filterEmpty = function(el) {
      return el.length !== 0
    }
    const activeRefsArray = activeRefs ? activeRefs.split('#').filter(filterEmpty) : []
    const mutualRefsArray = mutualRefs ? mutualRefs.split('#').filter(filterEmpty) : []
    const passiveRefsArray = passiveRefs ? passiveRefs.split('#').filter(filterEmpty) : []

    if (activeRefs || mutualRefs || passiveRefs || relationName) {
      let entityElem
      let entityId
      let listType
      for (let i = 0; i < activeRefsArray.length; i++) {
        entityElem = document.createElement('evt-named-entity-ref')
        entityId = activeRefsArray[i].trim()
        listType = '' // parsedData.getNamedEntityType(entityId)

        if (entityId && entityId !== '') {
          entityElem.setAttribute('data-entity-id', entityId)
        }
        // entityElem.setAttribute('data-entity-type', listType)
        entityElem.textContent = '#' + entityId
        relationText += entityElem.outerHTML + ' '
        activeText += entityElem.outerHTML.trim() + ', '
      }

      for (let j = 0; j < mutualRefsArray.length; j++) {
        if (j === 0 && activeRefs && activeRefs !== '') {
          relationText += '{{ \'AND\' | translate}} '
        }

        entityElem = document.createElement('evt-named-entity-ref')
        entityId = mutualRefsArray[j].trim()
        // listType = parsedData.getNamedEntityType(entityId)

        if (entityId && entityId !== '') {
          entityElem.setAttribute('data-entity-id', entityId)
        }
        // entityElem.setAttribute('data-entity-type', listType);
        entityElem.textContent = '#' + entityId
        relationText += entityElem.outerHTML + ' '
        mutualText += entityElem.outerHTML.trim() + ', '
      }

      relationText += relationName ? '<span class="relation-name">' + relationName + ' </span>' : ''

      for (let k = 0; k < passiveRefsArray.length; k++) {
        entityElem = document.createElement('evt-named-entity-ref')
        entityId = passiveRefsArray[k].trim()
        // listType = parsedData.getNamedEntityType(entityId)

        if (entityId && entityId !== '') {
          entityElem.setAttribute('data-entity-id', entityId)
        }
        // entityElem.setAttribute('data-entity-type', listType)
        entityElem.textContent = '#' + entityId
        relationText += entityElem.outerHTML + ' '
        passiveText += entityElem.outerHTML.trim() + ', '
      }
    }
    relationText += '</span>'
    relationText += parsedRelation && parsedRelation.innerHTML !== undefined ? parsedRelation.innerHTML : nodeElem.innerHTML

    // Update info in parsed named entities
    // Active roles
    // Add relation info to active elements
    for (let x = 0; x < activeRefsArray.length; x++) {
      const entityActive: NamedEntity = this.parsedNamedEntities.getNamedEntity(activeRefsArray[x].trim())
      if (entityActive && relationText !== '') {
        if (!entityActive.content.properties.relations) {
          entityActive.content.properties.relations = []
          entityActive.content.indexes.push('relations')
        }
        entityActive.content.properties.relations.push({
          text: '{{ \'LISTS.RELATION_ACTIVE_ROLE \' | translate:\'{relationType:"' + relationType + '"}\'}}: ' + relationText,
          attributes: {
            values: {},
            indexes: []
          }
        })
        entityActive.xmlSource += nodeElem.outerHTML.replace(/ xmlns="http:\/\/www\.tei-c\.org\/ns\/1\.0"/g, '')
      }
    }

    // Mutual roles
    for (let y = 0; y < mutualRefsArray.length; y++) {
      const entityMutual: NamedEntity = this.parsedNamedEntities.getNamedEntity(mutualRefsArray[y].trim())
      if (entityMutual && relationText !== '') {
        if (!entityMutual.content.properties.relations) {
          entityMutual.content.properties.relations = []
          entityMutual.content.indexes.push('relations')
        }
        entityMutual.content.properties.relations.push({
          text: '{{ \'LISTS.RELATION_MUTUAL_ROLE \' | translate:\'{relationType:"' + relationType + '"}\'}}: ' + relationText,
          attributes: {
            values: {},
            indexes: []
          }
        })
        entityMutual.xmlSource += nodeElem.outerHTML.replace(/ xmlns="http:\/\/www\.tei-c\.org\/ns\/1\.0"/g, '')
      }
    }

    // Passive roles
    for (let z = 0; z < passiveRefsArray.length; z++) {
      const entityPassive: NamedEntity = this.parsedNamedEntities.getNamedEntity(passiveRefsArray[z].trim())
      if (entityPassive && relationText !== '') {
        if (!entityPassive.content.properties.relations) {
          entityPassive.content.properties.relations = []
          entityPassive.content.indexes.push('relations')
        }
        entityPassive.content.properties.relations.push({
          text: '{{ \'LISTS.RELATION_PASSIVE_ROLE \' | translate:\'{relationType:"' + relationType + '"}\'}}: ' + relationText,
          attributes: {
            values: {},
            indexes: []
          }
        })
        entityPassive.xmlSource += nodeElem.outerHTML.replace(/ xmlns="http:\/\/www\.tei-c\.org\/ns\/1\.0"/g, '')
      }
    }

    // Add details to relation element
    relationEl.label = this.JsUtils.capitalize(relationEl.label + ': ' + relationText)
    relationEl.indexingLetter = relationEl.label.substr(0, 1).toLowerCase()

    if (activeText !== '' || mutualText !== '' || passiveText !== '') {
      const actors = []

      if (activeText !== '') {
        actors.push({
          attributes: {
            type: 'LISTS.RELATION_ACTIVE',
            _indexes: ['type']
          },
          text: activeText.slice(0, -2)
        })
      }

      if (mutualText !== '') {
        actors.push({
          attributes: {
            type: 'LISTS.RELATION_MUTUAL',
            _indexes: ['type']
          },
          text: mutualText.slice(0, -2)
        })
      }

      if (passiveText !== '') {
        actors.push({
          attributes: {
            type: 'LISTS.RELATION_PASSIVE',
            _indexes: ['type']
          },
          text: passiveText.slice(0, -2)
        })
      }

      relationEl.content.properties.actors = actors
      relationEl.content.indexes.push('actors')
    }


    this.parsedNamedEntities.addNamedEntityInCollection(collection, relationEl)
  }

  private parseCollectionData(el, collection: NamedEntityCollection): NamedEntityCollection {
    if (el.previousElementSibling && this.listsTags.headerTag.indexOf('<' + el.previousElementSibling.tagName + '>') >= 0) {
      collection.id = el.previousElementSibling.textContent.trim().replace(/\s/g, '')
      collection.title = el.previousElementSibling.textContent.trim()
    } else {
      const parentNode = el.parentNode
      let listId
      if (parentNode && parentNode.getAttribute(this.listsTags.idAttribute)) {
        listId = parentNode.getAttribute(this.listsTags.idAttribute).trim().replace(/\s/g, '')
        collection.id = listId
        collection.title = listId
      }
      if (parentNode && parentNode.getAttribute(this.listsTags.typeAttribute)) {
        let listTitle = parentNode.getAttribute(this.listsTags.typeAttribute).trim()
        if (!listId || listId === undefined) {
          collection.id = listTitle
        }
        listTitle = this.JsUtils.camelToSpace(listTitle)
        collection.title = (listTitle.substr(0, 1).toUpperCase() + listTitle.substr(1))
      }
    }
    return collection;
  }

  private parseEntity(nodeElem: any, listToParse: any, collection: NamedEntityCollection) {
    const contentDef = listToParse.contentDef
    const listDef = listToParse.listDef
    const contentForLabelDef = listToParse.contentForLabelDef
    let elId = nodeElem.getAttribute(this.listsTags.idAttribute)
    if (!elId || elId === '') {
      elId = this.parsedNamedEntities.generateNamedEntityId(collection, nodeElem)
    }
    // || evtParser.xpath(nodeElem);
    const el: NamedEntity = {
      id: elId,
      label: '',
      content: {
        properties: {},
        indexes: []
      },
      entityType: collection.listType,
      indexingLetter: elId.substr(0, 1).toLowerCase(),
      occurrences: undefined,
      xmlSource: nodeElem.outerHTML.replace(/ xmlns="http:\/\/www\.tei-c\.org\/ns\/1\.0"/g, '')
    }
    const elementForLabel = nodeElem.getElementsByTagName(contentForLabelDef.replace(/[<>]/g, ''));
    if (elementForLabel && elementForLabel.length > 0) {
      const parsedLabel = this.genericParser.parseXMLElement(elementForLabel[0], elementForLabel[0], {skip: '<evtNote>'})
      el.label = parsedLabel ? parsedLabel.innerHTML : elId;
    } else {
      el.label = elId;
    }
    for (const child of nodeElem.childNodes) {
      // Each child node of XML node will be saved in a JSON structure that looks like this:
      // element: {
      //    content : {
      //      'childNode_1': { 'text': 'Text with HTML of child 1', 'attributes': [ ] },
      //      'childNode_2': { 'text': 'Text with HTML of child 2', 'attributes': [ ] },
      //      'childNode_n': { 'text': 'Text with HTML of child n', 'attributes': [ ] },
      //      _indexes: [ 'childNode_1', 'childNode_2', 'childNode_n' ]
      //    }
      // }
      // The generic XML parser will transform the content of each node in an HTML element
      // with Tag Name as Class Name
      if (child.nodeType === 1) {
        if (contentForLabelDef.indexOf('<' + child.tagName + '>') >= 0 && child.childNodes && child.childNodes.length > 0) {
          for (const subChild of child.childNodes) {
            if (subChild.nodeType === 1) {
              this.parseAndAddContentToEntity(el, subChild, contentDef, listDef)
            }
          }
        } else {
          this.parseAndAddContentToEntity(el, child, contentDef, listDef)
        }
      }
    }
    return el
  }

  private parseAndAddContentToEntity(el: NamedEntity, child, contentDef, listDef) {
    const propertyName: string = child.tagName
    if (el.content.properties[propertyName] === undefined) {
      el.content.properties[propertyName] = [];
      el.content.indexes.push(propertyName);
    }
    let parsedChild
    if (contentDef.indexOf('<' + propertyName + '>') >= 0) {
      parsedChild = this.parseSubEntity(child, contentDef, listDef)
    } else {
      parsedChild = this.genericParser.parseXMLElement(child, child, {skip: '<evtNote>'})
    }
    el.content.properties[propertyName].push({
      text: parsedChild ? parsedChild.innerHTML : child.innerHTML,
      attributes: this.genericParser.parseElementAttributes(child)
    })
  }

  // NAMED ENTITIES OCCURRENCES
  private getPageIdFromHTMLString(HTMLstring: string) {
    const pbIdAttr = 'xml:id' // TODO: Add definition in tags
    const matchPbIdAttr = pbIdAttr + '=".*"'
    let sRegExPbIdAttr = new RegExp(matchPbIdAttr, 'ig')
    const pbHTMLString = HTMLstring.match(sRegExPbIdAttr)
    sRegExPbIdAttr = new RegExp(pbIdAttr + '=(?:"[^"]*"|^[^"]*$)', 'ig')
    const idAttr = pbHTMLString ? pbHTMLString[0].match(sRegExPbIdAttr) : undefined
    const pageId = idAttr ? idAttr[0].replace(/xml:id/, '').replace(/(=|\"|\')/ig, '') : ''
    return pageId
  }

  /**
   * This method will parse all the occurrences of a particular named entity..
   * - It will use regular expression to find the page breaks before a specific occurence
   * - For each page break identified, it will retrieve the detailed information already parsed
   *
   * @param docObj XML element representing the document to be parsed
   * @param refId id of named entity to handle
   *
   * @returns array of pages in which the given named entity appears.
   * Each page is structured as follows:
    <pre>
    let page = {
      pageId: ''.
      pageLabel: '',
      docId: '',
      docLabel: ''
    }
    </pre>
   *
   * @author CDP
   */
  parseEntitiesOccurrencesInDocument(docObj: any, refId: string): Array<NamedEntityOccurrence> {
    // doc = docObj && docObj.content ? docObj.content : undefined
    // docHTML = doc ? doc.outerHTML : undefined
    const docHTML = docObj ? docObj.children[0].outerHTML : undefined // TEMP
    const occurrences: Array<NamedEntityOccurrence> = []
    if (docHTML && refId && refId !== '') {
      const match = '<pb(.|[\r\n])*?\/>(.|[\r\n])*?(?=#' + refId + ')'
      const sRegExInput = new RegExp(match, 'ig')
      const matches = docHTML.match(sRegExInput)
      const totMatches = matches ? matches.length : 0
      for (let i = 0; i < totMatches; i++) {
        // Since JS does not support lookbehind I have to get again all <pb in match and take the last one
        const matchOnlyPb = '<pb(.|[\r\n])*?\/>'
        const sRegExOnlyPb = new RegExp(matchOnlyPb, 'ig')
        const pbList = matches[i].match(sRegExOnlyPb)
        const pbString = pbList && pbList.length > 0 ? pbList[pbList.length - 1] : ''
        const pageId = this.getPageIdFromHTMLString(pbString)
        if (pageId) {
          const pageObj = undefined // = parsedData.getPage(pageId);
          occurrences.push({
            pageId: pageId,
            pageLabel: pageObj ? pageObj.label : pageId,
            docId: docObj ? docObj.value : '',
            docLabel: docObj ? docObj.label : ''
          })
        }
      }
    }
    return occurrences
  }

  parseEntityOccurrences(documentsCollection: any[], entityId: string): Array<NamedEntityOccurrence> {
    // documentsCollection = this.baseData.getXMLDocuments() // parsedData.getDocuments(),
    // documentsIndexes = documentsCollection._indexes || [],
    let totOccurrences: Array<NamedEntityOccurrence> = []
    for (let i = 0; i < documentsCollection.length; i++) { // documentsCollection.documentsIndexes.length
      const currentDoc = documentsCollection[i] // documentsCollection[documentsIndexes[i]]
      const docPages = this.parseEntitiesOccurrencesInDocument(currentDoc, entityId)
      totOccurrences = totOccurrences.concat(docPages)
    }
    this.parsedNamedEntities.updateNamedEntityOccurrences(entityId, totOccurrences)
    return totOccurrences
  }
}
