import {Injectable} from '@angular/core';

import {XmlTagsService} from './xml-tags.service'
import {DOMUtilsService} from '../../../utils/dom-utils.service'
import {JsUtilsService} from '../../../utils/js-utils.service'

import {GenericParserService} from './generic-parser.service'
import {StructureService} from '../../parsed-data/services/structure.service'

// Models
import {PageTags, DocTags, GenericTagSet} from '../models/xml-tags/structure-tags'
import {Document, Page} from '../../parsed-data/models/edition-structure'

@Injectable()
export class StructureParserService {
  private docTags: DocTags
  private pageTags: PageTags
  private contentEditionDef: string

  constructor(
    private xmlTags: XmlTagsService,
    private DOMUtils: DOMUtilsService,
    private JsUtils: JsUtilsService,
    private parsedStructure: StructureService,
    private genericParser: GenericParserService) {
  }
  /**
     * Parse the documents of a given XML document
     * and store them in StructureService for future retrievements.
   *
     * @param currentDocument XML element to parse
     *
     * @author CDP
     */
  parseDocuments(currentDocument: any) {
    const docTags = this.xmlTags.getTagSet('docs')
    let defDocElement: string
    let defContentEdition: string
    for (const definition of docTags.tagNames) {
      if (!defContentEdition) {
        defDocElement = definition.docTag
        if (currentDocument.querySelectorAll(defDocElement).length > 0) {
          defContentEdition = definition.editionTag
          this.xmlTags.updatePropertyInSet('docs', 'docDef', defDocElement)
          this.xmlTags.updateTagSet('contentEditionDef', defContentEdition)
        }
      }
    }

    const frontDef = this.xmlTags.getTagSet('frontDef')
    const biblDef: GenericTagSet = this.xmlTags.getTagSet('biblDef')

    // TODO parsedData.setCriticalEditionAvailability(currentDocument.find(config.listDef.replace(/[<>]/g, '')).length > 0);

    const docElements = currentDocument.querySelectorAll(defDocElement)
    for (const element of docElements) {
      const newDoc: Document = {
        id: element.getAttribute('xml:id') || this.parsedStructure.generateDocumentId(element),
        label: element.getAttribute('n') || this.parsedStructure.generateDocumentLabel(),
        title: element.getAttribute('n') || this.parsedStructure.generateDocumentTitle(),
        content: element,
        front: undefined,
        pages: [], // Pages will be added later
        attributes: this.genericParser.parseElementAttributes(element)
      };
      const docFront = element.querySelectorAll(frontDef.replace(/[<\/>]/ig, ''))
      if (docFront && docFront[0]) {
        const frontElem = docFront[0].cloneNode(true)
        const biblRefs = frontElem.querySelectorAll(biblDef.tagName.replace(/[<\/>]/ig, ''))
        if (biblRefs) {
          for (let i = biblRefs.length - 1; i >= 0; i--) {
            const evtBiblElem = document.createElement('evt-bibl-elem')
            const biblElem = biblRefs[i]
            const biblId = biblElem.getAttribute(biblDef.idAttr) || this.DOMUtils.xpath(biblElem).substr(1)
            evtBiblElem.setAttribute('data-bibl-id', biblId)
            biblElem.parentNode.replaceChild(evtBiblElem, biblElem)
          }
        }
        const parsedContent = this.genericParser.parseXMLElement(element, frontElem, {
          skip: biblDef.tagName + '<evt-bibl-elem>'
        })
        const frontAttributes = this.genericParser.parseElementAttributes(frontElem)
        newDoc.front = {
          attributes: frontAttributes,
          parsedContent: parsedContent && parsedContent.outerHTML ? parsedContent.outerHTML.trim() : '',
          originalContent: frontElem.outerHTML
        }
      }
      this.parsedStructure.addDocument(newDoc)
      this.parsePages(element, newDoc.id)
      // TODO
      //      if (config.defaultEdition !== 'critical' || !parsedData.isCriticalEditionAvailable()) {
      //        // Split pages works only on diplomatic/interpretative edition
      //        // In critical edition, text will be splitted into pages for each witness
      //        config.defaultEdition = 'diplomatic';
      const editionTag: string = this.JsUtils.removeTagBrackets(defContentEdition)
      for (const editionElement of element.querySelectorAll(editionTag)) {
        // editionElement.innerHTML = parser.splitLineBreaks(element, defContentEdition);
        this.splitPages(editionElement, newDoc.id, editionTag)
      }
      //      }
    }
    console.log('## PAGES ##', this.parsedStructure.getPages())
    console.log('## Documents ##', this.parsedStructure.getDocuments())
    return this.parsedStructure.getDocuments()
  }
  /**
   * Parse the pages of a given edition document in a given XML document
   * and store them in StructureService for future retrievements.
   *
   * @param currentDocument XML element to parse
   * @param docId Id of the document to analyze and to whom add parsed pages
   *
   * @author CDP
   */
  parsePages(currentDocument: any, docId: string) {
    const pageTags: PageTags = this.xmlTags.getTagSet('pages')
    const pageElements = currentDocument.querySelectorAll(this.JsUtils.removeTagBrackets(pageTags.tagName))
    for (const element of pageElements) {
      const idAttr: string = element.getAttribute(pageTags.idAttr)
      const edAttr: string = element.getAttribute(pageTags.edAttr)
      const numAttr: string = element.getAttribute(pageTags.numAttr)
      const newPage: Page = {
        id: idAttr,
        label: numAttr,
        n: numAttr,
        title: numAttr,
        source: undefined,
        text: undefined,
        docs: [],
        attributes: this.genericParser.parseElementAttributes(element),
        xmlSource: this.DOMUtils.getOuterHTML(element)
      }
      if (idAttr && idAttr !== '') {
        newPage.id = idAttr
      } else if (edAttr && numAttr) {
        newPage.id = edAttr + '-' + numAttr
      } else {
        newPage.id = this.parsedStructure.generatePageId(element)
      }
      if (numAttr) {
        newPage.label = newPage.n = numAttr
        newPage.title = numAttr
      } else {
        newPage.label = newPage.n = this.parsedStructure.generatePageLabel()
        newPage.title = this.parsedStructure.generatePageTitle()
      }

      // Get image source URL
      if (element.getAttribute(pageTags.facsAttr)) {
        newPage.source = element.getAttribute(pageTags.facsAttr);
      } else {
        // TODO: handle other cases (e.g. <surface>)
        newPage.source = '';
      }
      this.parsedStructure.addPage(newPage, docId)
    }
  }

  /**
   * Parse a given XML document and split it into pages.
   * It will uses regular expression to divide the XML into pieces
   * and then balance the generated XHTML in order to handle the last page break.
   * Finally it stores the result into StructureService for future retrievements.
   *
   * @param docElement XML element to parse
   * @param docId Id of the current edition document being parsed (used to store data)
   * @param defContentEdition String representing the definition of the starting point of the edition
   * @author CDP
   */
  splitPages(docElement: any, docId: string, defContentEdition: string) {
    const contentEditionDef: string = this.JsUtils.removeTagBrackets(this.xmlTags.getTagSet('contentEditionDef'))
    const pageTags: PageTags = this.xmlTags.getTagSet('pages')
    const pageDef: string = this.JsUtils.removeTagBrackets(pageTags.tagName)
    let matchOrphanText = '<' + contentEditionDef
    matchOrphanText += '(.|[\r\n])*?(?=<' + pageDef + ')'
    const sRegExInputOrphanText = new RegExp(matchOrphanText, 'ig')
    const matchesOrphanText = docElement.outerHTML.match(sRegExInputOrphanText)
    if (matchesOrphanText && matchesOrphanText.length > 0) {
      const previousDoc = this.parsedStructure.getPreviousDocument(docId)
      if (previousDoc && previousDoc.pages && previousDoc.pages.length > 0) {
        const parentPageId = previousDoc.pages[previousDoc.pages.length - 1]
        if (parentPageId && parentPageId !== '') {
          this.parsedStructure.setPageText(parentPageId, docId, 'original', matchesOrphanText[0])
        }
      }
    }
    const match = '<' + pageDef + '(.|[\r\n])*?(?=(<' + pageDef + '|<\/' + defContentEdition + '>))'
    const sRegExInput = new RegExp(match, 'ig')
    const matches = docElement.outerHTML.match(sRegExInput)
    const totMatches = matches ? matches.length : 0
    for (let i = 0; i < totMatches; i++) {
      const matchPbIdAttr = pageTags.idAttr + '=".*"'
      let sRegExPbIdAttr = new RegExp(matchPbIdAttr, 'ig')
      const pbHTMLString = matches[i].match(sRegExPbIdAttr)
      sRegExPbIdAttr = new RegExp(pageTags.idAttr + '=(?:"[^"]*"|^[^"]*$)', 'ig')
      const idAttr = pbHTMLString ? pbHTMLString[0].match(sRegExPbIdAttr) : undefined
      const pageId = idAttr ? idAttr[0].replace(/xml:id/, '').replace(/(=|\"|\')/ig, '') : ''
      if (pageId && pageId !== '') {
        this.parsedStructure.setPageText(pageId, docId, 'original', matches[i])
      }
    }
  }
}
