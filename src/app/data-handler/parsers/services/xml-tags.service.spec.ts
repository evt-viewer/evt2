import { TestBed, inject } from '@angular/core/testing';

import { XmlTagsService } from './xml-tags.service';

describe('XmlTagsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XmlTagsService]
    });
  });

  it('should be created', inject([XmlTagsService], (service: XmlTagsService) => {
    expect(service).toBeTruthy();
  }));
});
