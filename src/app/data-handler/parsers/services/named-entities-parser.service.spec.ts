import { TestBed, inject } from '@angular/core/testing';

import { NamedEntitiesParserService } from './named-entities-parser.service';

describe('NamedEntitiesParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NamedEntitiesParserService]
    });
  });

  it('should be created', inject([NamedEntitiesParserService], (service: NamedEntitiesParserService) => {
    expect(service).toBeTruthy();
  }));
});
