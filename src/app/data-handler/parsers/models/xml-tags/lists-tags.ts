export interface ListsTags {
  extContainer: string,
  whatToParse: Array<ListToParse>,
  idAttribute: string,
  typeAttribute: string,
  headerTag: string
}

interface ListToParse {
  listDef: string,
  listType: string,
  contentDef: string,
  contentForLabelDef: string
}

