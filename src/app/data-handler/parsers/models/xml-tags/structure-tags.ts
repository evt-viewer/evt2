export interface PageTags {
  tagName: string,
  idAttr: string,
  numAttr: string,
  edAttr: string,
  facsAttr: string
}

export interface DocTags {
  tagNames: Array<DocTagName>,
  docDef: string,
  idAttr: string,
  numAttr: string
}

export interface DocTagName {
  docTag: string,
  editionTag: string
}

export interface GenericTagSet {
  tagName: string,
  idAttr: string
}
