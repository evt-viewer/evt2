export interface RelationsTags {
  idAttribute: string,
  listTag: string,
  relationTag: string,
  nameAttribute: string,
  activeAttribute: string,
  passiveAttribute: string,
  mutualAttribute: string,
  typeAttribute: string
}
