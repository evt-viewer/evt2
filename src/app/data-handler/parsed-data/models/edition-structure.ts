import { Attributes } from '../../models/shared';
export interface Page {
  id: string,
  label: string,
  n: string,
  title: string,
  source: any,
  text: {
    [docId: string]: {
      [editionLevel: string]: string
    }
  },
  docs: string[],
  attributes: Attributes,
  xmlSource: string
}

export interface Document {
  id: string,
  label: string,
  title: string,
  content: any,
  front: DocumentFront,
  pages: string[],
  attributes: Attributes
}

export interface DocumentFront {
  attributes: Attributes,
  parsedContent: any,
  originalContent: any
}
