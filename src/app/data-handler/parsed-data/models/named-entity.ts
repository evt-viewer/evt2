import { Attributes } from '../../models/shared';
export interface NamedEntity {
  id: string,
  label: string,
  content: {
    properties: {[name: string]: Array<NamedEntityContent>},
    indexes: string[]
  },
  entityType: string,
  indexingLetter: string,
  occurrences: NamedEntityOccurrence[],
  xmlSource: string
}

export interface NamedEntityCollection {
  id: string,
  indexes: string[],
  listKeys: string[],
  title: string,
  listType: string,
  listIcon: string,
  entities: {
    [letter: string]: {
      elements: {
        [id: string]: NamedEntity
      },
      indexes: string[]
    }
  }
}

export interface NamedEntityContent {
  text: string,
  attributes: Attributes
}

export interface NamedEntityOccurrence {
  pageId: string,
  pageLabel: string,
  docId: string,
  docLabel: string
}
