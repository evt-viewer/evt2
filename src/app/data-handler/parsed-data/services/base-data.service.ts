import {Injectable} from '@angular/core'

import 'rxjs/add/operator/toPromise'

import {ConfigService} from '../../../config/config.service'
import {JsUtilsService} from '../../../utils/js-utils.service'
import {XMLUtilsService} from '../../../utils/xml-utils.service'

import {NamedEntitiesParserService} from '../../parsers/services/named-entities-parser.service'
import {StructureParserService} from '../../parsers/services/structure-parser.service'

@Injectable()
export class BaseDataService {
  baseData: any = {}
  state: any = {
    XMLDocuments: [],
    XMLStrings: []
  }
  parseXml: any

  constructor(
    private EVTnamedEntitiesParser: NamedEntitiesParserService,
    private EVTstructureParser: StructureParserService,
    private XMLUtils: XMLUtilsService) { }

  private addXMLDocument(doc: any) {
    const docElements = this.XMLUtils.parseXml(doc)
    if (docElements.documentElement.nodeName === 'TEI') {
      this.state.XMLStrings.push(doc)
      this.state.XMLDocuments.push(docElements)
      this.EVTstructureParser.parseDocuments(docElements)
      this.EVTnamedEntitiesParser.parseEntities(docElements)
    } else {
      // _console.error('Something wrong with the XML')
    }
  }

  addXMLString(xmlString: any) {
    this.addXMLDocument(xmlString)
  }

  getXMLDocuments() {
    return this.state.XMLDocuments
  }

  getXMLStrings() {
    return this.state.XMLStrings
  }
}
