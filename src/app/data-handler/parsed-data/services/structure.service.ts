import {Injectable} from '@angular/core';

import {DOMUtilsService} from '../../../utils/dom-utils.service'
import {Document, Page} from '../models/edition-structure'

@Injectable()
export class StructureService {
  private documentsCollection: {
    documents: {
      [docId: string]: Document
    }
    indexes: string[]
  }

  private pagesCollection: {
    pages: {
      [pageId: string]: Page
    }
    indexes: string[]
  }
  constructor(private DOMUtils: DOMUtilsService) {
    this.documentsCollection = {
      documents: {},
      indexes: []
    }
    this.pagesCollection = {
      pages: {},
      indexes: []
    }
  }
  // DOCUMENTS //

  generateDocumentId(docElement) {
    const xPathId = this.DOMUtils.xpath(docElement)
    return xPathId && xPathId !== '' ? xPathId.substr(1) : 'doc_' + this.documentsCollection.indexes.length
  }

  generateDocumentLabel(): string {
    return 'Doc ' + (this.documentsCollection.indexes.length + 1)
  }

  generateDocumentTitle(): string {
    return 'Document ' + (this.documentsCollection.indexes.length + 1)
  }
  getDocuments(): any {
    return this.documentsCollection
  }
  getDocument(docId: string): Document {
    return this.documentsCollection.documents[docId]
  }
  getPreviousDocument(docId: string): Document {
    const currentDocIndex = this.documentsCollection.indexes.indexOf(docId)
    const previousId = this.documentsCollection.indexes[currentDocIndex - 1]
    return this.documentsCollection.documents[previousId]
  }

  addDocument(docObj) {
    let docId = docObj.id
    if (docId === '') {
      docId = docObj.value = 'doc_' + (this.documentsCollection.indexes.length + 1)
    }
    if (this.documentsCollection.documents[docId] === undefined) {
      this.documentsCollection.indexes.push(docId)
      this.documentsCollection.documents[docId] = docObj
    }
  }

  // PAGES
  generatePageId(pageElement?: any) {
    if (pageElement) {
      const xPathId = this.DOMUtils.xpath(pageElement)
      return xPathId && xPathId !== '' ? xPathId.substr(1) : 'page_' + (this.pagesCollection.indexes.length + 1)
    } else {
      return 'page_' + (this.pagesCollection.indexes.length + 1)
    }
  }
  generatePageLabel(): string {
    return 'Page ' + (this.pagesCollection.indexes.length + 1)
  }

  generatePageTitle(): string {
    return 'Page ' + (this.pagesCollection.indexes.length + 1)
  }

  addPage(pageObj: Page, docId: string) {
    let pageId = pageObj.id
    if (pageObj.id === '') {
      pageId = pageObj.id = this.generatePageId()
    }
    if (this.pagesCollection.pages[pageId] === undefined) {
      pageObj.docs = [docId];
      this.pagesCollection.indexes.push(pageId)
      this.pagesCollection.pages[pageId] = pageObj
      // _console.log('parsedData - addPage ', page);
    } else {
      const parsedPage = this.pagesCollection.pages[pageId]
      if (parsedPage.docs && parsedPage.docs.indexOf(docId) < 0) {
        parsedPage.docs.push(docId)
      }
    }

    if (docId && docId !== '' && this.documentsCollection.documents[docId] !== undefined) {
      this.documentsCollection.documents[docId].pages.push(pageId)
    }
  }
  getPages() {
    return this.pagesCollection
  }
  getPage(pageId: string) {
    return this.pagesCollection.pages[pageId]
  }

  setPageText(pageId: string, docId: string, editionLevel: string, HTMLtext: string) {
    const pageObj = this.pagesCollection.pages[pageId]
    if (pageObj) {
      if (!pageObj.text) {
        pageObj.text = {}
      }
      const pageDocObj = pageObj.text[docId]
      if (pageDocObj !== undefined && pageDocObj[editionLevel] !== undefined) {
        pageDocObj[editionLevel] += HTMLtext
      } else if (pageDocObj !== undefined) {
        pageDocObj[editionLevel] = HTMLtext
      } else {
        pageObj.text[docId] = {}
        pageObj.text[docId][editionLevel] = HTMLtext
      }
      if (pageObj.docs && pageObj.docs.indexOf(docId) < 0) {
        pageObj.docs.push(docId)
      }
    }
  }

  getPageText(pageId: string, docId: string, editionLevel: string) {
    const pageObj = this.pagesCollection.pages[pageId]
    if (pageObj && pageObj.text && pageObj.text[docId]) {
      return pageObj.text[docId][editionLevel]
    }
    return undefined
  }

  getPageImage(pageId: string) {
    const images = []

    let i = 0
    while (i < images.length && images[i].page !== pageId) {
      i++
    }
    // return images[i];
    return {}
  }
}
