import { TestBed, inject } from '@angular/core/testing';

import { NamedEntitiesService } from './named-entities.service';

describe('NamedEntitiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NamedEntitiesService]
    });
  });

  it('should be created', inject([NamedEntitiesService], (service: NamedEntitiesService) => {
    expect(service).toBeTruthy();
  }));
});
