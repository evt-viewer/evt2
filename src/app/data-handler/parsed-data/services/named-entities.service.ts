import {Injectable} from '@angular/core'
import 'rxjs/add/operator/map'

import {NamedEntityCollection, NamedEntity, NamedEntityOccurrence} from '../models/named-entity'
import {DOMUtilsService} from '../../../utils/dom-utils.service'

@Injectable()
export class NamedEntitiesService {
  private collections: {
    [id: string]: NamedEntityCollection
  } = {}
  private collectionsIndexes: string[] = []
  private entities: {
    [id: string]: {
      collectionId: string,
      indexingLetter: string
    }
  } = {}

  constructor(
    private DOMUtils: DOMUtilsService) {

  }

  generateCollectionId(listElement: any) {
    const xPathId = this.DOMUtils.xpath(listElement)
    return xPathId && xPathId !== '' ? xPathId.substr(1) : 'list_' + this.collectionsIndexes.length
  }

  generateNamedEntityId(collection: NamedEntityCollection, namedEntityElement: any) {
    const collectionId = collection.id
    if (!this.collections || this.collections[collectionId] === undefined) {
      this.addNamedEntitiesCollection(collection)
    }
    const xPathId = this.DOMUtils.xpath(namedEntityElement)
    return xPathId && xPathId !== '' ? xPathId.substr(1) : collectionId + '_entity_' + this.collections[collectionId].indexes.length
  }

  // Adders
  addNamedEntitiesCollection(collection: NamedEntityCollection) {
    const collectionId = collection.id
    if (!this.collections || this.collections[collectionId] === undefined) {
      const listType = collection && collection.listType ? collection.listType : 'generic',
        listIcon = this.getNamedEntityTypeIcon(listType)

      this.collections[collectionId] = {
        id: collectionId,
        indexes: [],
        listKeys: [],
        title: collection && collection.title ? collection.title : '',
        listType: listType,
        listIcon: listIcon,
        entities: {}
      }

      this.collectionsIndexes.push(collectionId)
    }
  }

  addNamedEntityInCollection(collection: NamedEntityCollection, namedEntity: NamedEntity) {
    const collectionId = collection.id
    if (!this.collections || this.collections[collectionId] === undefined) {
      this.addNamedEntitiesCollection(collection)
    }
    const indexingLetter = namedEntity.indexingLetter
    if (this.collections[collectionId].entities[indexingLetter] === undefined) {
      this.collections[collectionId].entities[indexingLetter] = {
        elements: {},
        indexes: []
      }
      this.collections[collectionId].listKeys.push(indexingLetter)
    }
    const entityId = namedEntity.id
    this.collections[collectionId].entities[indexingLetter].elements[entityId] = namedEntity
    this.collections[collectionId].entities[indexingLetter].indexes.push(entityId)

    this.entities[entityId] = {
      collectionId: collectionId,
      indexingLetter: indexingLetter
    }
    this.collections[collectionId].indexes.push(entityId)
  }
  // Getters
  getNamedEntityTypeIcon(entityType: string) {
    let icon
    switch (entityType) {
      case 'place':
      case 'placeName':
        icon = 'fa-map-marker'
        break
      case 'person':
      case 'pers':
      case 'persName':
        icon = 'fa-user'
        break
      case 'org':
      case 'orgName':
        icon = 'fa-users'
        break
      case 'relation':
        icon = 'fa-share-alt'
        break
      default:
        icon = 'fa-list-ul'
        break
    }
    return icon
  }

  getCollections() {
    return this.collections
  }
  getCollectionsIndexes() {
    return this.collectionsIndexes
  }
  getEntities() {
    return this.entities
  }

  getCollectionById(collectionId: string) {
    return this.collections[collectionId]
  }

  getCollectionByIdAndLetter(collectionId: string, indexingLetter: string) {
    return this.collections[collectionId].entities[indexingLetter]
  }

  getNamedEntity(entityId: string) {
    let namedEntity
    if (entityId) {
      const namedEntityRefs = this.entities[entityId]
      if (namedEntityRefs) {
        const collectionId = namedEntityRefs.collectionId
        const indexingLetter = namedEntityRefs.indexingLetter
        if (collectionId !== undefined && indexingLetter !== undefined &&
          this.collections[collectionId] !== undefined &&
          this.collections[collectionId].entities[indexingLetter] !== undefined) {
          namedEntity = this.collections[collectionId].entities[indexingLetter].elements[entityId]
        }
      }
    }
    return namedEntity
  }

  getNamedEntityType(entityId: string) {
    const collectionId = entityId && this.entities[entityId] ? this.entities[entityId].collectionId : undefined
    const collectionObj = this.getCollectionById(collectionId)
    return collectionObj ? collectionObj.listType : 'generic'
  }

  updateNamedEntityOccurrences(entityId: string, occurrences: Array<NamedEntityOccurrence>) {
    const namedEntity: NamedEntity = this.getNamedEntity(entityId)
    if (namedEntity) {
      namedEntity.occurrences = occurrences
    }
  }
}
