export class Attributes {
  values: {
    [name: string]: string
  }
  indexes: string[]
}
