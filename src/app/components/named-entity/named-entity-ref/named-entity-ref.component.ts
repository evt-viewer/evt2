import { Component, OnInit, ElementRef  } from '@angular/core';

@Component({
  selector: 'evt-named-entity-ref',
  templateUrl: './named-entity-ref.component.html',
  styleUrls: ['./named-entity-ref.component.scss']
})
export class NamedEntityRefComponent implements OnInit {
  private entityId: string

  private visible: boolean = false

  constructor(public elementRef: ElementRef) {
    var native = this.elementRef.nativeElement
    this.entityId = native.getAttribute("data-entity-id")
  }

  ngOnInit() {
  }

  goToEntityInList(){
    this.visible = !this.visible
  }
}
