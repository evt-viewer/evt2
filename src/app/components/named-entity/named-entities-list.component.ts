import {Component, OnInit, Input} from '@angular/core'

import {NamedEntitiesService} from '../../data-handler/parsed-data/services/named-entities.service'
import {NamedEntityCollection} from '../../data-handler/parsed-data/models/named-entity'

@Component({
    selector: 'evt-named-entities-list',
    templateUrl: './named-entities-list.component.html',
    styleUrls: ['_named-entities-list.component.scss']
})
export class NamedEntitiesListComponent implements OnInit {
    @Input() listId: string

    /* All elements in list */
    allElements: string[] = []
    /* Elements in selected letter */
    elementsInListKey: string[] = []
    /* Elements shown */
    visibleElements: string[] = []
    /* Selected letter */
    selectedLetter: string
    /* List of available letters */
    listKeys: string[] = []
    /* Type of List */
    listType: string
    constructor(public parsedNamedEntities: NamedEntitiesService) {
    }

    ngOnInit() {
        let parsedCollection: NamedEntityCollection = this.parsedNamedEntities.getCollectionById(this.listId)
        let selectedLetter = parsedCollection ? parsedCollection.listKeys[0] : undefined
        this.elementsInListKey = this.getVisibleElements(selectedLetter)
        this.visibleElements = this.elementsInListKey ? this.elementsInListKey.slice(0, 40) : []
        this.listKeys = parsedCollection ? parsedCollection.listKeys : []
        this.selectedLetter = this.listKeys.length > 0 ? this.listKeys[0] : ''

        this.allElements = parsedCollection ? parsedCollection.indexes : []
        this.listType = parsedCollection ? parsedCollection.listType : 'generic'
    }

    /**
     * <p>Show (and initialize) 10 more elements on screen.</p>
     */
    private loadMoreElements() : void {
        let last = this.visibleElements ? this.visibleElements.length : 0
        let i = 0
        while (i < 10 && i < this.elementsInListKey.length) {
            let newElement = this.elementsInListKey[last + i]
            if (newElement && this.visibleElements.indexOf(newElement) <= 0) {
                this.visibleElements.push(newElement)
            }
            i++
        }
    }

    // getVisibleElements.
    // Support function. Retrieve visible element for a certain list at a certain letter
    private getVisibleElements(letter:string) : string[] {
        let visibleElements : string[] = []
        let parsedElements : any = {}
        if (letter) {
            parsedElements = this.parsedNamedEntities.getCollectionByIdAndLetter(this.listId, letter)
            visibleElements = parsedElements ? parsedElements.indexes : []
        }
        return visibleElements
    }

    /**
     * <p>Retrieve the list of elements, indexed at given letter.</p>
     * @param letter indexing letter to select
     */
    selectLetter(letter: string) : void {
        this.selectedLetter = letter
        this.elementsInListKey = this.getVisibleElements(letter)
        this.visibleElements = this.elementsInListKey ? this.elementsInListKey.slice(0, 40) : []
    }

    getListTypeClass() {
        return 'list-type-' + this.listType
    }
}
