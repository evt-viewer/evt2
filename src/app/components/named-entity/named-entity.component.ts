import {Component, OnInit, Input} from '@angular/core'

import {JsUtilsService} from '../../utils/js-utils.service'

import {NamedEntitiesParserService} from '../../data-handler/parsers/services/named-entities-parser.service'
import {NamedEntitiesService} from '../../data-handler/parsed-data/services/named-entities.service'
import {NamedEntity, NamedEntityContent} from '../../data-handler/parsed-data/models/named-entity'
import {BaseDataService} from '../../data-handler/parsed-data/services/base-data.service'

import {Language, Translation} from 'angular-l10n';

@Component({
  selector: 'evt-named-entity',
  templateUrl: './named-entity.component.html',
  styleUrls: ['./_named-entity.component.scss']
})
export class NamedEntityComponent extends Translation implements OnInit {
  @Language() lang: string
  @Input() entityId: string

  entity: NamedEntity = {
    id: '',
    label: '',
    content: {
      properties: {},
      indexes: []
    },
    entityType: '',
    indexingLetter: '',
    occurrences: undefined,
    xmlSource: ''
  }
  location: string

  moreInfoOpened: boolean
  moreInfoTabs: {
    tabs: {
      [name: string]: any
    },
    indexes: string[]
  } = {
    tabs: {},
    indexes: []
  }
  subContentOpened: string

  entityTypeIcon: string

  opened: boolean

  constructor(
    public JsUtils: JsUtilsService,
    private parsedNamedEntities: NamedEntitiesService,
    private namedEntitiesParser: NamedEntitiesParserService,
    private baseData: BaseDataService) {
    super();
  }

  ngOnInit() {
    this.entity = this.parsedNamedEntities.getNamedEntity(this.entityId)
    this.moreInfoOpened = false
    this.opened = false
    this.location = ''
    this.entityTypeIcon = this.parsedNamedEntities.getNamedEntityTypeIcon(this.entity.entityType)

    // Set tabs
    this.moreInfoTabs.indexes.push('moreInfo')
    this.moreInfoTabs.tabs.moreInfo = {label: 'NAMED_ENTITIES.MORE_INFO'}

    if (this.entity.entityType !== 'relation') {
      this.moreInfoTabs.indexes.push('occurrences')
      this.moreInfoTabs.tabs.occurrences = {label: 'NAMED_ENTITIES.OCCURRENCES'}
    }

    this.moreInfoTabs.indexes.push('xmlSource')
    this.moreInfoTabs.tabs.xmlSource = {label: 'NAMED_ENTITIES.XML'}
  }

  getEntityContent(entity: NamedEntity, property: string): Array<NamedEntityContent> {
     return entity.content.properties[property]
  }

  toggle() {
    if (this.location !== 'mainText') {
      this.toggleSubContent('moreInfo')
      this.opened = !this.opened
    }
  }

  toggleOccurrences() {
    if (!this.entity.occurrences) {
      // this.entity.occurrences = namedEntity.getOccurrences(this.entityId)
    }
    this.toggleSection('occurrencesOpened')
  }

  toggleSection(sectionName: string) {
    this.subContentOpened = sectionName
  }

  toggleMoreInfo() {
    this.moreInfoOpened = !this.moreInfoOpened
    this.toggleSection('moreInfoOpened')
  }

  goToOccurrence(occurrence: any) {
    // TODO Go to occurrence
  }

  toggleSubContent(subContentName: string) {
    if (subContentName === 'occurrences' && !this.entity.occurrences) {
      const documentsCollection = this.baseData.getXMLDocuments() // parsedData.getDocuments(),
      this.entity.occurrences = this.namedEntitiesParser.parseEntityOccurrences(documentsCollection, this.entityId)
    }
    if (this.subContentOpened !== subContentName) {
      this.subContentOpened = subContentName
    } else {
      this.subContentOpened = ''
    }
  };

  // Pin Tool Functions
  isPinAvailable(): boolean {
    // return config.toolPinAppEntries
    return true
  }
  isPinned() {
    // return evtPinnedElements.isPinned(this.entityId)
    return false
  }

  getPinnedState() {
    return this.isPinned() ? 'pin-on' : 'pin-off'
  }

  togglePin() {
    if (this.isPinned()) {
      // evtPinnedElements.removeElement({id: this.entityId, type: 'namedEntity_' + this.entity.entityType})
    } else {
      // evtPinnedElements.addElement({id: this.entityId, type: 'namedEntity_' + this.entity.entityType})
    }
  }

  isCurrentPageDoc(occurrence: any) {
    //    const currentDoc = evtInterface.getState('currentDoc')
    //    const currentPage = evtInterface.getState('currentPage')
    //    return (currentDoc === occurrence.docId && currentPage === occurrence.pageId)
  }
}
