import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Language, Translation} from "angular-l10n";

@Component({
    selector: 'evt-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['_dialog.component.scss']
})
export class DialogComponent extends Translation implements OnInit {
    @Language() lang: string

    @Input() type: string
    @Input() title: string
    @Input() noScroll: boolean

    @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>()

    constructor() {
        super()
        this.noScroll = false
    }

    ngOnInit() {
    }

    emitClose() {
        this.close.emit(true)
    }
}
