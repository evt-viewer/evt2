import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Language, Translation, LocaleService} from "angular-l10n";

@Component({
    selector: 'evt-main-menu',
    templateUrl: 'main-menu.component.html',
    styleUrls: ['_main-menu.component.scss']
})
export class MainMenuComponent extends Translation implements OnInit {
    @Language() lang: string

    @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>()
    @Output() itemPressed: EventEmitter<string> = new EventEmitter<string>()

    XMLDownloadEnabled: boolean
    namedEntitiesListsAvailable: boolean

    constructor(public locale: LocaleService) {
        super()
        this.XMLDownloadEnabled = true // TODO: Retrieve from configs
        this.namedEntitiesListsAvailable = true // TODO: Retrieve from configs and parsed data
    }

    ngOnInit() {
    }

    openGlobalDialogInfo() {
        // TODO: Open Dialog with Global Project Info
        this.itemPressed.emit('globalInfo')
        this.close.emit(true)
    }

    openGlobalDialogLists() {
        // TODO: Open Global Dialog with Entities Lists
        this.itemPressed.emit('entitiesList')
        this.close.emit(true)
    }

    generateBookmark() {
        // TODO: Generate Bookmark
        this.itemPressed.emit('bookmark')
        this.close.emit(true)
    }

    downloadXML() {
        // TODO: window.open(evtInterface.getProperty('dataUrl'), '_blank');
        console.log('Download XML...')
        this.close.emit(true)
    }

    getAvailableLanguages(): string[] {
        return ['en', 'it']
    }

    getCurrentLanguage(): string {
        return this.locale.getCurrentLanguage()
    }

    setLanguage(langKey: string) {
        this.locale.setCurrentLanguage(langKey)
        this.close.emit(true)
    }
}
