import {Component, OnInit, Input} from '@angular/core';
import {Language, Translation} from "angular-l10n";

@Component({
    selector: 'evt-tabs-container',
    templateUrl: './tabs-container.component.html',
    styleUrls: ['_tabs-container.component.scss']
})
export class TabsContainerComponent extends Translation implements OnInit {
    @Language() lang: string
    @Input() tabs: {
        indexes: string[],
        contents: {
            [id: string]: {
                label: string,
                name: string,
                icon?: string,
                content: string,
                scrollDisabled: boolean
            }
        }
    }
    @Input() orientation: string

    @Input() subContentOpened: string

    constructor() {
        super()
        this.orientation = this.orientation || 'vertical'
    }
    ngOnInit() {
        if (!this.subContentOpened ) {
            this.subContentOpened = this.tabs && this.tabs.indexes.length > 0 ? this.tabs.indexes[0] : ''
        }
    }


    /**
     * Open/close a given tab.
     * @param subContentName Name of tab to open/close
     */
    toggleSubContent(subContentName: string) {
        this.subContentOpened = this.subContentOpened !== subContentName ? subContentName : ''
    }

    getOrientation() {
        return this.orientation
    }

    getLabelIcon(tab: string) {
        return this.tabs && this.tabs.contents && this.tabs.contents[tab] ? this.tabs.contents[tab].icon : ''
    }
}
