import {Component, OnInit, Input} from '@angular/core';
import {Language, Translation} from "angular-l10n";

@Component({
    selector: 'evt-button',
    templateUrl: 'button.component.html',
    styleUrls: ['_button.component.scss']
})
export class ButtonComponent extends Translation implements OnInit {
    @Language() lang: string

    @Input() title: string
    @Input() label: string
    @Input() icon: string
    @Input() type: string
    @Input() value: string
    @Input() iconPos: string

    @Input() invertedColor: boolean
    @Input() noBackground: boolean

    @Input() callback: Function

    constructor() {
        super()
    }

    ngOnInit() {
        if (!this.iconPos) this.iconPos = 'left'
        if (!this.callback) this.callback = this.internalCallback
    }

    private internalCallback(){
        console.log("Missing callback...")
    }

    getIcon() {
        let evtIcon = ''
        switch (this.icon.toLowerCase()) {
            case 'add':
                evtIcon = 'icon-evt_add'
                break
            case 'color-legend':
                evtIcon = 'icon-evt_color-legend'
                break
            case 'filter':
            case 'filters':
                evtIcon = 'icon-evt_filter'
                break
            case 'font-size':
                evtIcon = 'icon-evt_font-size'
                break
            case 'font-size-minus':
                evtIcon = 'icon-evt_font-size-minus-alt'
                break
            case 'font-size-plus':
                evtIcon = 'icon-evt_font-size-plus-alt'
                break
            case 'font-size-reset':
                evtIcon = 'icon-evt_font-size-reset-alt'
                break
            case 'heatmap':
                evtIcon = 'icon-evt_heatmap-alt'
                break
            case 'info':
                evtIcon = 'icon-evt_info'
                break
            case 'list':
                evtIcon = 'icon-evt_list'
                break
            case 'menu':
            case 'menu-vert':
                evtIcon = 'icon-evt_more-vert'
                break
            case 'mode-imgtxt':
                evtIcon = 'icon-evt_imgtxt'
                break
            case 'mode-txttxt':
                evtIcon = 'icon-evt_txttxt'
                break
            case 'mode-critical':
                evtIcon = 'icon-evt_txt'
                break
            case 'mode-collation':
                evtIcon = 'icon-evt_collation'
                break
            case 'mode-bookreader':
                evtIcon = 'icon-evt_bookreader'
                break
            case 'pin':
                evtIcon = 'icon-evt_pin-alt-on'
                break
            case 'pin-off':
                evtIcon = 'icon-evt_pin-off'
                break
            case 'pin-on':
                evtIcon = 'icon-evt_pin-on'
                break
            case 'remove':
                evtIcon = 'icon-evt_close'
                break
            case 'search':
                evtIcon = 'icon-evt_search'
                break
            case 'bookmark':
                evtIcon = 'icon-evt_bookmark'
                break
            case 'thumb':
            case 'thumbs':
            case 'thumbnail':
            case 'thumbnails':
                evtIcon = 'icon-evt_thumbnails'
                break
            case 'v-align':
                evtIcon = 'icon-evt_align'
                break
            case 'witnesses':
                evtIcon = 'icon-evt_books'
                break
        }
        return evtIcon
    }
}
