import {Injectable} from '@angular/core'
import {HttpClientModule, HttpClient, HttpHeaders} from '@angular/common/http'
import 'rxjs/add/operator/toPromise'

import {ConfigService} from '../config/config.service'
import {BaseDataService} from '../data-handler/parsed-data/services/base-data.service'

@Injectable()
export class CommunicationService {
  communication: any = {}
  defaults: any = {
    mode: 'xml',

    errorMsgs: {
      '404': {
        title: 'File not found',
        msg: 'Something wrong during loading file'
      }
    }
  }
  currentError: {
    code: string,
    title: string,
    msg: string
  }

  constructor(
    public config: ConfigService,
    private http: HttpClient,
    private baseData: BaseDataService) {}

  getExternalConfig(url: string) {
      return this.http.get<any>(url)
        .toPromise()
        .then((data:any) => {
           this.config.extendDefault(data)
        })
        .catch(error => {
           this.err('Something wrong while loading configuration file', error)
        });
  }

  getData(url: string) {
      return this.http.get(url, {responseType: 'text'})
        .toPromise()
        .then((data:string) => {
            //console.log('DATA RECEIVED', data)
            if (data.indexOf('<TEI') >= 0 && data.indexOf('</TEI>') >= 0) { // is TEI
              this.baseData.addXMLString(data)
            } else {
              // TODO: JSON?
            }
        })
        .catch(error => {
            if (this.defaults.errorMsgs[error]) {
              this.err(this.defaults.errorMsgs[error].msg + ' "' + url + '"', error)
            } else {
              this.err = error
            }
        });
  }

  getError = function() {
    return this.currentError
  }

  updateError(newError: any) {
    this.currentError = newError
  }

  err(msg: string, code: string) {
    // _console.log('# ERROR '+code+' # ' + msg)
    code = code !== undefined ? code : ''
    const newError = {
      code: code,
      msg: msg,
      title: this.defaults.errorMsgs[code] ? 'Error ' + code + ' - ' + this.defaults.errorMsgs[code].title : 'Communication error ' + code
    }
    this.updateError(newError)

    // TODO: Show dialog with error
  }
}
